package com.biom4st3r.coderecipes.mixin;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.inventory.CraftingResultInventory;
import net.minecraft.screen.CraftingScreenHandler;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.world.World;

import com.biom4st3r.coderecipes.BioLogger;
import com.biom4st3r.coderecipes.CraftingInventoryContext;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(CraftingScreenHandler.class)
public abstract class CraftInventoryHandlerMxn {
    private static final BioLogger logger = new BioLogger("CraftInventoryContextProvider");
    @Inject(
        at = @At("HEAD"), 
        method = "updateResult", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private static void provideContextToCraftingInventory(ScreenHandler screenHandler, World world, PlayerEntity player, CraftingInventory craftingInventory, CraftingResultInventory resultInventory, CallbackInfo ci)
    {
        logger.debug("providing context");
        ((CraftingInventoryContext)craftingInventory).setPlayer(player);
    }
    
}