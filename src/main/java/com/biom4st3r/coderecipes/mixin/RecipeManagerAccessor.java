package com.biom4st3r.coderecipes.mixin;

import java.util.Map;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.util.Identifier;

/**
 * RecipeManagerAccessor
 */
@Mixin(RecipeManager.class)
public interface RecipeManagerAccessor {

    @Accessor("recipes")
    public Map<RecipeType<?>, Map<Identifier, Recipe<?>>> getRecipeMap();

    @Accessor("recipes")
    public void setRecipeMap(Map<RecipeType<?>, Map<Identifier, Recipe<?>>> map);
    
}