package com.biom4st3r.coderecipes.mixin;

import java.util.Map;

import com.biom4st3r.coderecipes.BioLogger;
import com.biom4st3r.coderecipes.api.RecipeHelper;
import com.google.common.collect.Maps;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.util.Identifier;

@Mixin(RecipeManager.class)
public abstract class RecipeManagerMxn
{
    BioLogger recipe_logger = new BioLogger("ProgrammicRecipes");

    @Shadow
    private Map<RecipeType<?>, Map<Identifier, Recipe<?>>> recipes;

    @Inject(at = @At("RETURN"),method="apply")
    public void createRecipeMap(CallbackInfo ci)
    {
        recipes = Maps.newHashMap(recipes);
        int size = 0;
        for(RecipeType<?> type : recipes.keySet())
        {
            try
            {
                size += RecipeHelper.recipeMapHolder.get(type).size();
            }
            catch(NullPointerException e)
            {
                
            }
        }
        recipe_logger.log(String.format("Adding %s Recipes from Programmatic Recipes", size));
        RecipeHelper._addRecipesToMap(recipes);
    }
}