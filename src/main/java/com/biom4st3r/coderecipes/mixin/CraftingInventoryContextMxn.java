package com.biom4st3r.coderecipes.mixin;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.CraftingInventory;

import com.biom4st3r.coderecipes.CraftingInventoryContext;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;

/**
 * CraftingInventoryContextMxn
 */
@Mixin(CraftingInventory.class)
public abstract class CraftingInventoryContextMxn implements CraftingInventoryContext {

    @Unique
    public PlayerEntity player;
    // @Unique
    // private static final BioLogger logger = new BioLogger("CraftingInventoryContextMxn");
    // @Inject(at = @At("TAIL"),method = "canPlayerUse")
    // public void contextProvider(PlayerEntity pe,CallbackInfoReturnable<Boolean> ci)
    // {
    //     this.player = pe;
    // }

    // @Inject(
    //     at = @At(value = "RETURN"),
    //     method = "canPlayerUse",
    //     cancellable = false,
    //     locals = LocalCapture.NO_CAPTURE
    // )
    // public void contextProvider2(PlayerEntity pe, CallbackInfoReturnable<Boolean> ci)
    // {
    //     if(player == null) 
    //     {
    //         logger.debug("setting Player");
    //         player = pe;
    //     }
    // }

    @Override
    public PlayerEntity getPlayer() {
        return player;
    }

    @Override
    public void setPlayer(PlayerEntity pe) {
        player = pe;
    }



    
}