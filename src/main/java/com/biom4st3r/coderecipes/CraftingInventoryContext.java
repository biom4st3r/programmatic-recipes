package com.biom4st3r.coderecipes;

import net.minecraft.entity.player.PlayerEntity;

/**
 * CraftingInventoryContext
 */
public interface CraftingInventoryContext {

    public PlayerEntity getPlayer();
    public void setPlayer(PlayerEntity pe);
}