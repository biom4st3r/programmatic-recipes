package com.biom4st3r.coderecipes.api;

import com.google.common.annotations.Beta;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.BlastingRecipe;
import net.minecraft.recipe.CampfireCookingRecipe;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.SmeltingRecipe;
import net.minecraft.recipe.SmokingRecipe;
import net.minecraft.recipe.StonecuttingRecipe;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;

/**
 * Recipes<p>
 * "group" is for grouping crafting recipes such as planks or logs
 * 
 */
public final class Recipes {

    public Item[] fromTag(Tag<Item> tag)
    {
        return (Item[]) tag.values().toArray();
    }

    /**
     * 
     * @param id recipe identifier
     * @param group 
     * @param output item to be output
     * @param exp 
     * @param cookTime time in ticks
     * @param items input ktem
     * @return returns a new recipe
     */
    public static Recipe<?> newSmokingRecipe(Identifier id,String group,Item output,float exp,int cookTime, Item... items)
    {
        return new SmokingRecipe(id, group, Ingredient.ofItems(items), new ItemStack(output), exp, cookTime);
    }

    /**
     * 
     * @param id recipe identifier
     * @param group 
     * @param output relies on the SmeltingOutput fix to suppy more than one item on output
     * @param exp 
     * @param cookTime time in ticks
     * @param items input ktem
     * @return returns a new recipe
     */
    @Beta
    public static Recipe<?> newSmokingRecipe(Identifier id,String group,ItemStack output,float exp,int cookTime, Item... items)
    {
        return new SmokingRecipe(id, group, Ingredient.ofItems(items), output, exp, cookTime);
    }
    /**
     * 
     * @param id recipe identifier
     * @param group 
     * @param output item to be output
     * @param exp 
     * @param cookTime time in ticks
     * @param items input ktem
     * @return returns a new recipe
     */
    public static Recipe<?> newSmeltingRecipe(Identifier id,String group,Item output,float exp,int cookTime, Item... items)
    {
        return new SmeltingRecipe(id, group, Ingredient.ofItems(items), new ItemStack(output), exp, cookTime);
    }

    /**
     * 
     * @param id recipe identifier
     * @param group 
     * @param output relies on the SmeltingOutput fix to suppy more than one item on output
     * @param exp 
     * @param cookTime time in ticks
     * @param items input ktem
     * @return returns a new recipe
     */
    @Beta
    public static Recipe<?> newSmeltingRecipe(Identifier id,String group,ItemStack output,float exp,int cookTime, Item... items)
    {
        return new SmeltingRecipe(id, group, Ingredient.ofItems(items), output, exp, cookTime);
    }

    /**
     * 
     * @param id recipe identifier
     * @param group 
     * @param output item to be output
     * @param exp 
     * @param cookTime time in ticks
     * @param items input ktem
     * @return returns a new recipe
     */
    public static Recipe<?> newBlastingRecipe(Identifier id,String group,Item output,float exp,int cookTime, Item... items)
    {
        return new BlastingRecipe(id, group, Ingredient.ofItems(items), new ItemStack(output), exp, cookTime);
    }

    /**
     * 
     * @param id recipe identifier
     * @param group 
     * @param output relies on the SmeltingOutput fix to suppy more than one item on output
     * @param exp 
     * @param cookTime time in ticks
     * @param items input ktem
     * @return returns a new recipe
     */
    @Beta
    public static Recipe<?> newBlastingRecipe(Identifier id,String group,ItemStack output,float exp,int cookTime, Item... items)
    {
        return new BlastingRecipe(id, group, Ingredient.ofItems(items), output, exp, cookTime);
    }
    /**
     * 
     * @param id recipe identifier
     * @param group 
     * @param output item to be output
     * @param exp 
     * @param cookTime time in ticks
     * @param items input ktem
     * @return returns a new recipe
     */
    public static Recipe<?> newCampFireRecipe(Identifier id,String group,Item output,float exp,int cookTime, Item... items)
    {
        return new CampfireCookingRecipe(id, group, Ingredient.ofItems(items), new ItemStack(output), exp, cookTime);
    }

    /**
     * 
     * @param id recipe identifier
     * @param group 
     * @param output relies on the SmeltingOutput fix to suppy more than one item on output
     * @param exp 
     * @param cookTime time in ticks
     * @param items input ktem
     * @return returns a new recipe
     */
    @Beta
    public static Recipe<?> newCampFireRecipe(Identifier id,String group,ItemStack output,float exp,int cookTime, Item... items)
    {
        return new CampfireCookingRecipe(id, group, Ingredient.ofItems(items), output, exp, cookTime);
    }

    /**
     * 
     * @param id recipe identifier
     * @param group 
     * @param output item to be output
     * @param exp 
     * @param cookTime time in ticks
     * @param items input ktem
     * @return returns a new recipe
     */
    public static Recipe<?> newStoneCuttingRecipe(Identifier id,String group,ItemStack output, Item... items)
    {
        return new StonecuttingRecipe(id, group, Ingredient.ofItems(items), output);
    }

    /**
     * 
     * @return a ShaplessRecipeBuilder
     */
    public static ShapelessRecipeBuilder newShapelessRecipe()
    {
        return new ShapelessRecipeBuilder();
    }

    /**
     * 1 2 3<p>
     * 4 5 6<p>
     * 7 8 9<p>
     * <p>
     * 1 2<p>
     * 3 4
     * @param width width of table
     * @param height height of table
     * @return a ShapedRecipeBuilder
     */
    public static ShapedRecipeBuilder newShapedRecipe(int width, int height)
    {
        return new ShapedRecipeBuilder(width, height);
    }

    @Beta
    public static ReferencedShapedRecipeBuilder newRefShapedRecipe(int width, int height)
    {
        return new ReferencedShapedRecipeBuilder(width, height);
    }

    @Beta
    public static ReferencedShapelessRecipeBuilder newRefShapelessRecipe()
    {
        return new ReferencedShapelessRecipeBuilder();
    }

}