package com.biom4st3r.coderecipes.api;

import java.util.List;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.ShapelessRecipe;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;

import com.biom4st3r.coderecipes.BioLogger;
import com.biom4st3r.coderecipes.CraftingInventoryContext;

/**
 * ReferencedShapelessRecipe
 */
public class ReferencedShapelessRecipe extends ShapelessRecipe {


    RecipeOutputModifier outputModifier;
    public ReferencedShapelessRecipe(Identifier id, String group, ItemStack outputSample, DefaultedList<Ingredient> input,RecipeOutputModifier outputModifier) {
        super(id, group, outputSample, input);
        this.outputModifier = outputModifier;
    }

    private List<ItemStack> getInput(CraftingInventory craftingInv)
    {
        DefaultedList<ItemStack> stacks = DefaultedList.ofSize(craftingInv.size(), ItemStack.EMPTY);
        for(int i = 0; i < craftingInv.size(); i++)
        {
            stacks.set(i, craftingInv.getStack(i).copy());
        }
        return stacks;
    }

    private static final BioLogger logger = new BioLogger("ReferencedShapelessRecipe");
    @Override
    public ItemStack craft(CraftingInventory craftingInventory) {
        logger.debug("playerNull %s", ((CraftingInventoryContext)craftingInventory).getPlayer() == null);
        ItemStack modded_output = this.outputModifier.apply(this,getInput(craftingInventory),((CraftingInventoryContext)craftingInventory).getPlayer());
        if(modded_output == null)
        {
            return this.getOutput().copy();
        }
        return modded_output.copy();
    }

    @Override
    public ItemStack getOutput() {
        return super.getOutput().copy();
    }

    
}