package com.biom4st3r.coderecipes.api;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.ShapedRecipe;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;

/**
 * ShapedRecipeBuilder
 */
public class ShapedRecipeBuilder {

    int width;
    int height;

    public ShapedRecipeBuilder(int width, int height)
    {
        list = DefaultedList.ofSize(width*height,Ingredient.EMPTY);
        this.width = width;
        this.height = height;
    }
    DefaultedList<Ingredient> list;
    
    public Recipe<?> build(Identifier id,String group,ItemStack output) {
        return new ShapedRecipe(id, group, width, height, list, output);
    }

    
    public ShapedRecipeBuilder addIngredient1(Item... items) {
        list.set(0, Ingredient.ofItems(items));
        return this;
    }

    
    public ShapedRecipeBuilder addIngredient2(Item... items) {
        list.set(1, Ingredient.ofItems(items));
        return this;
    }

    
    public ShapedRecipeBuilder addIngredient3(Item... items) {
        list.set(2, Ingredient.ofItems(items));
        return this;
    }

    
    public ShapedRecipeBuilder addIngredient4(Item... items) {
        list.set(3, Ingredient.ofItems(items));
        return this;
    }

    
    public ShapedRecipeBuilder addIngredient5(Item... items) {
        list.set(4, Ingredient.ofItems(items));
        return this;
    }

    
    public ShapedRecipeBuilder addIngredient6(Item... items) {
        list.set(5, Ingredient.ofItems(items));
        return this;
    }

    
    public ShapedRecipeBuilder addIngredient7(Item... items) {
        list.set(6, Ingredient.ofItems(items));
        return this;
    }

    
    public ShapedRecipeBuilder addIngredient8(Item... items) {
        list.set(7, Ingredient.ofItems(items));
        return this;
    }

    
    public ShapedRecipeBuilder addIngredient9(Item... items) {
        list.set(8, Ingredient.ofItems(items));
        return this;
    }
    
}