package com.biom4st3r.coderecipes.api;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.RecipeType;
import net.minecraft.util.Identifier;

/**
 * RecipeEntryBuilder
 */
public class RecipeHelper {
    public static Map<RecipeType<?>,Map<Identifier,Recipe<?>>> recipeMapHolder = Maps.newHashMap();
    /**
     * @deprecated for clarity. please use {@link RecipeHelper#register(Identifier, RecipeType, Recipe)}
     * @param id
     * @param recipeType
     * @param recipe
     */
    @Deprecated
    public static void addRecipeToMap(Identifier id, RecipeType<?> recipeType, Recipe<?> recipe)
    {
        Map<Identifier,Recipe<?>> recipeTypeMap = recipeMapHolder.get(recipeType);
        try
        {
            recipeTypeMap.put(id, recipe);
        }
        catch(NullPointerException e)
        {
            recipeTypeMap = Maps.newLinkedHashMap();
            recipeTypeMap.put(id, recipe);
        }
        recipeMapHolder.put(recipeType, recipeTypeMap);
    }

    public static void register(Identifier id, RecipeType<?> recipeType, Recipe<?> recipe)
    {
        addRecipeToMap(id, recipeType,  recipe);
    }

    public static void _addRecipesToMap(Map<RecipeType<?>, Map<Identifier, Recipe<?>>> vanillaMap)
    {

        Map<Identifier, Recipe<?>> vanillaTypeMap;

        for(RecipeType<?> type : vanillaMap.keySet())
        {
            vanillaTypeMap = vanillaMap.get(type);
            if(vanillaTypeMap instanceof ImmutableMap)
            {
                vanillaTypeMap = Maps.newHashMap(vanillaTypeMap);
            }
            if(recipeMapHolder.get(type) == null)
            {
                continue;
            }
            if(recipeMapHolder.get(type).size() > 0)
            {
                vanillaTypeMap.putAll(recipeMapHolder.get(type));
            }
            vanillaMap.put(type, vanillaTypeMap);
        }
    }
}