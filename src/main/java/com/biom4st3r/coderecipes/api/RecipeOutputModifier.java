package com.biom4st3r.coderecipes.api;

import java.util.List;

import org.jetbrains.annotations.Nullable;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Recipe;

/**
 * Tri-Function of (Recipe<?> recipe,List<ItemStack> list, PlayerEntity pe)ItemStack;<p>
 * Returning null will output original Recipe.getOutput()
 */
public interface RecipeOutputModifier {
    @Nullable
    ItemStack apply(Recipe<?> recipe,List<ItemStack> list, PlayerEntity pe);
}