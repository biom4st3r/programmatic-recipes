package com.biom4st3r.coderecipes.api;

import java.util.List;
import java.util.logging.Logger;

import com.biom4st3r.coderecipes.ModInit;
import com.google.common.collect.Lists;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.recipe.ShapelessRecipe;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;

/**
 * ShapelessRecipeBuilder
 */
public class ShapelessRecipeBuilder {
    Logger logger = Logger.getLogger(ModInit.MODID);

    List<Ingredient> list = Lists.newArrayListWithCapacity(9);
    /**
     * 
     * @param id Identifier for your recipes. Usually refers to the recipe.json
     * @param group Recipe book group
     * @param output
     * @return
     */
    public Recipe<?> build(Identifier id,String group,ItemStack output) {
        DefaultedList<Ingredient> l = DefaultedList.ofSize(list.size(), Ingredient.EMPTY);
        if(list.size() > 9)
        {
            logger.warning(String.format("%s recipe has to many ingredients. Size is %s and should not exceed 9", id.toString(),list.size()));
        }
        for(int i = 0; i < list.size(); i++)
        {
            l.set(i, list.get(i));
        }
        return new ShapelessRecipe(id, group, output, l);
    }

    public ShapelessRecipeBuilder addIngredient(Item... items) {
        list.add(Ingredient.ofItems(items));
        return this;
    }
}