package com.biom4st3r.coderecipes.api;

import java.util.List;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.ShapedRecipe;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;

import com.biom4st3r.coderecipes.CraftingInventoryContext;

/**
 * ReferencedShapelessRecipe
 */
public class ReferencedShapedRecipe extends ShapedRecipe {

    // BiFunction<List<ItemStack>,PlayerEntity,ItemStack>
    RecipeOutputModifier outputModifier;
    public ReferencedShapedRecipe(Identifier id, String group, int width, int height,
            DefaultedList<Ingredient> ingredients, ItemStack sampleOutput, RecipeOutputModifier output) {
        super(id, group, width, height, ingredients, sampleOutput);
        this.outputModifier = output;
    }

    private List<ItemStack> getInput(CraftingInventory craftingInv)
    {
        DefaultedList<ItemStack> stacks = DefaultedList.ofSize(craftingInv.size(), ItemStack.EMPTY);
        for(int i = 0; i < craftingInv.size(); i++)
        {
            stacks.set(i, craftingInv.getStack(i).copy());
        }
        return stacks;
    }

    @Override
    public ItemStack getOutput() {
        return super.getOutput().copy();
    }


    @Override
    public ItemStack craft(CraftingInventory craftingInventory) {
        ItemStack modded_output = this.outputModifier.apply(this,getInput(craftingInventory),((CraftingInventoryContext)craftingInventory).getPlayer());
        if(modded_output == null)
        {
            return this.getOutput().copy();
        }
        return modded_output.copy();
    }

    
}