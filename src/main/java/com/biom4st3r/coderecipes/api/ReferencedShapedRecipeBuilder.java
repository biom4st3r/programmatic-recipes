package com.biom4st3r.coderecipes.api;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.Ingredient;
import net.minecraft.recipe.Recipe;
import net.minecraft.util.Identifier;
import net.minecraft.util.collection.DefaultedList;

import com.google.common.annotations.Beta;

/**
 * ReferencedShapedRecipeBuilder
 */
@Beta
public class ReferencedShapedRecipeBuilder {

    int width;
    int height;
    DefaultedList<Ingredient> list;
    public ReferencedShapedRecipeBuilder(int width, int height)
    {
        list = DefaultedList.ofSize(width*height,Ingredient.EMPTY);
        this.width = width;
        this.height = height;
    }
    
    /**
     * 
     * @param id
     * @param group
     * @param sampleOutput
     * @param outputModifier privides a list of the ItemStacks currently in the craftingTable and returns whatever is output
     * @return
     */
    public Recipe<?> build(Identifier id,String group,ItemStack sampleOutput,RecipeOutputModifier outputModifier) {
        return new ReferencedShapedRecipe(id, group, width, height, list, sampleOutput,outputModifier);
    }

    
    public ReferencedShapedRecipeBuilder addIngredient1(Item... items) {
        list.set(0, Ingredient.ofItems(items));
        return this;
    }

    
    public ReferencedShapedRecipeBuilder addIngredient2(Item... items) {
        list.set(1, Ingredient.ofItems(items));
        return this;
    }

    
    public ReferencedShapedRecipeBuilder addIngredient3(Item... items) {
        list.set(2, Ingredient.ofItems(items));
        return this;
    }

    
    public ReferencedShapedRecipeBuilder addIngredient4(Item... items) {
        list.set(3, Ingredient.ofItems(items));
        return this;
    }

    
    public ReferencedShapedRecipeBuilder addIngredient5(Item... items) {
        list.set(4, Ingredient.ofItems(items));
        return this;
    }

    
    public ReferencedShapedRecipeBuilder addIngredient6(Item... items) {
        list.set(5, Ingredient.ofItems(items));
        return this;
    }

    
    public ReferencedShapedRecipeBuilder addIngredient7(Item... items) {
        list.set(6, Ingredient.ofItems(items));
        return this;
    }

    
    public ReferencedShapedRecipeBuilder addIngredient8(Item... items) {
        list.set(7, Ingredient.ofItems(items));
        return this;
    }

    
    public ReferencedShapedRecipeBuilder addIngredient9(Item... items) {
        list.set(8, Ingredient.ofItems(items));
        return this;
    }

    
}