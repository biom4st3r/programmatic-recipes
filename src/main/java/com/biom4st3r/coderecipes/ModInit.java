package com.biom4st3r.coderecipes;

import net.minecraft.block.Blocks;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtList;
import net.minecraft.nbt.NbtString;
import net.minecraft.recipe.RecipeType;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;

import com.biom4st3r.coderecipes.api.RecipeHelper;
import com.biom4st3r.coderecipes.api.Recipes;
import com.google.common.collect.Sets;

/**
 * ModInit
 */
public class ModInit implements ModInitializer, ClientModInitializer {

    public static final String MODID = "programmatic";

    @Override
    public void onInitializeClient() {
    }

    @Override
    public void onInitialize() {
        if(!FabricLoader.getInstance().isDevelopmentEnvironment()) return;
        Identifier id = new Identifier(MODID,"stone_dirt_to_diamond");
        RecipeHelper.register(
            id,
            RecipeType.CRAFTING, 
            Recipes.newShapelessRecipe()
                .addIngredient(Items.STONE)
                .addIngredient(Items.DIRT)
                .build(id, MODID, new ItemStack(Items.DIAMOND)));
        RecipeHelper.register((id=new Identifier(MODID,"multiitem-smithing-output")), RecipeType.SMELTING, Recipes.newSmeltingRecipe(id, MODID, new ItemStack(Items.DIAMOND,7), 4, 100, Items.DIAMOND_CHESTPLATE));
        RecipeHelper.register(
            (id=new Identifier(MODID, "shaped_stone_to_diamond")),
            RecipeType.CRAFTING, 
            Recipes.newShapedRecipe(2, 2)
                .addIngredient1(Items.DIRT)
                .addIngredient2(Items.STONE)
                .addIngredient3(Items.STONE)
                .addIngredient4(Items.DIRT)
                .build(id, MODID, new ItemStack(Items.DIAMOND, 3)));
        RecipeHelper.register((id=new Identifier(MODID, "smelt_dirt_to_diamond")), RecipeType.SMELTING, Recipes.newSmeltingRecipe(id, MODID, Items.DIAMOND, 2, 100, Items.DIRT));
        RecipeHelper.register((id=new Identifier(MODID,"campfire_dirt_to_diamond")), RecipeType.CAMPFIRE_COOKING, Recipes.newCampFireRecipe(id, MODID, Items.DIAMOND_AXE, 7, 100, Items.DIRT));
        RecipeHelper.register((id=new Identifier(MODID,"cake_to_sugar")), RecipeType.SMOKING, Recipes.newSmokingRecipe(id, MODID, new ItemStack(Items.SUGAR,2), 5, 50, Items.CAKE));
        RecipeHelper.register(
            (id = new Identifier(MODID, "referenced_craft_with_player_name")), 
            RecipeType.CRAFTING, 
            Recipes.newRefShapelessRecipe()
            .addIngredient(Items.WOODEN_AXE)
            .addIngredient(Items.PAPER)
            .build(id, MODID, new ItemStack(Items.WOODEN_AXE), (recipe,items,player)->
            {
                ItemStack modded = items.stream().filter((is)->is.getItem() == Items.WOODEN_AXE).findFirst().get();
                NbtList lt = new NbtList();
                lt.add(NbtString.of(Text.Serializer.toJson(new LiteralText(player.getEntityName()))));
                modded.getOrCreateSubTag("display").put("Lore", lt);
                return modded;
            }));
        RecipeHelper.register(
            (id = new Identifier(MODID, "referenced_craft_with_block_underplayer")), 
            RecipeType.CRAFTING, 
            Recipes.newRefShapelessRecipe()
            .addIngredient(Items.COAL_BLOCK)
            .addIngredient(Items.COAL_BLOCK)
            .build(id, MODID, new ItemStack(Items.COAL_BLOCK,2), (recipe,items,player)->
            {
                if(player.world.getBlockState(player.getBlockPos().down()).getBlock() == Blocks.PISTON)
                {
                    return new ItemStack(Items.DIAMOND);
                }
                return null;
            }));
        RecipeHelper.register(
            (id = new Identifier(MODID, "referenced_craft_shaped_firebook")), 
            RecipeType.CRAFTING, 
            Recipes.newRefShapedRecipe(2,2)
            .addIngredient1(Items.COAL_BLOCK)
            .addIngredient2(Items.BOOK,Items.ENCHANTED_BOOK)
            .build(id, MODID, new ItemStack(Items.ENCHANTED_BOOK), (recipe,items,player)->
            {
                ItemStack op = items.stream().filter(is->is.getItem() != Items.COAL_BLOCK).findFirst().get();
                op = op.getItem() == Items.BOOK ? recipe.getOutput() : op;
                for(Enchantment e : Sets.newHashSet(Enchantments.FLAME,Enchantments.FIRE_ASPECT))
                {
                    op.addEnchantment(e, e.getMaxLevel());
                }
                return op;
            }));
    }
}